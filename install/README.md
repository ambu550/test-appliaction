# Ubuntu

## Install Java 17
```
sudo apt install openjdk-17-jdk
```

## Install NodeJS 18 (for tests)
```
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
```
### Insatall Allure command line
```
sudo npm install -g allure-commandline --save-dev
```


## Install Docker
```
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce

sudo apt install docker-ce;
sudo systemctl status docker;
sudo usermod -aG docker ${USER};
su - ${USER};
id -nG
```

## Install Docker-compose
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```