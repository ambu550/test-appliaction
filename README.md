## Application for tests examples
# Report Page

=> https://test-appliaction-ambu550-ffc42d699c2dfd959b7f682fb639ba7e80e611.gitlab.io/#behaviors

# Test Application

- [x] [Back-End](https://gitlab.com/ambu550/test-appliaction/-/tree/main/springboot-backend)
- [x] [Front](https://gitlab.com/ambu550/test-appliaction/-/tree/main/react-frontend)


## For base work need to install
[Install](https://gitlab.com/ambu550/test-appliaction/-/tree/main/install/README.md)


## Start application
```shell script
./start-app.sh
```

- Docker
- Gitlab ci/cd
- Allure

- Java 17
- NodeJS

- Spring Boot
- RabbitMQ
- Mysql
- React

# Tests examples

- [x] [Back-End java tests with java-testhelper-core](https://gitlab.com/ambu550/test-appliaction/-/tree/main/springboot-backend/auto-tests)  
- [x] [Front tests with Playwright-ts](https://gitlab.com/ambu550/test-appliaction/-/tree/main/react-frontend/tests-front-playwright)


## java-testhelper-core
=> https://java-testhelper-core-ambu550-1f8723467f72abd0047dfa797c79a09304.gitlab.io
##### examples
=> https://java-testhelper-tests-ambu550-1e56a841797c51161915e4c76af127e54.gitlab.io/

## playwright
=> https://playwright.dev/docs/intro


## base-app get from:
https://github.com/RameshMF/ReactJS-Spring-Boot-CRUD-Full-Stack-App

*and yes, there is a typo in the title, well done if you noticed)