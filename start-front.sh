#!/bin/bash

set -e

docker-compose -f ./react-frontend/build-app.yml up --force-recreate
docker-compose -f ./react-frontend/build-app.yml down --remove-orphans
docker-compose -f ./react-frontend/docker-compose.yml up -d

echo "Front started"
