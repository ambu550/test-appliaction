const {defineConfig} = require('cypress')
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
// import allureWriter from "@shelex/cypress-allure-plugin/writer";

module.exports = defineConfig({
    viewportHeight: 1280,
    viewportWidth: 720,
    defaultCommandTimeout: 3000,
    requestTimeout: 30000,
    e2e: {
        fixturesFolder: 'cypress/fixtures',
        baseUrl: 'http://client:3000',
        supportFile: 'cypress/support/e2e.js',
        screenshotsFolder: "_output/screenshots",
        videosFolder: "_output/videos",
        testIsolation: true,
        setupNodeEvents(on, config) {
            allureWriter(on, config);
            return config;
        },
        env: {
            api_server: "http://localhost:8080",
            allureResultsPath: "_output/allure-results",
            allure: true,
            allureAddVideoOnPass: true,
            allureLogCypress: true,
            allureAttachRequests: true
        },
    }
});