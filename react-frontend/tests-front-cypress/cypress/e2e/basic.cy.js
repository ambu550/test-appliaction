import {mock_employees} from "../reequests/mock_employees";
import {verify} from "../reequests/verify";


describe('Basic tests', {}, () => {

    beforeEach(() => {
        cy.allure().epic("[front] Tests for UI(cypress)");
    })

    it('onPage', () => {
        mock_employees.employeesList()
        cy.visit('/')

        verify.waitRequest('@employeesList')
    })

})

