class MockEmployees {


    employeesList(fixt = '__default', code = 200) {
        cy.allure().startStep('Create mock');
        cy.intercept(
            'GET',
            Cypress.env('api_server') + '/api/v1/employees',
            (req) => {
                req.reply({fixture: fixt, statusCode: code})
            }).as('employeesList')
        cy.allure().endStep();
    }


}

/**
 * mock Object
 */
export const mock_employees = new MockEmployees();



