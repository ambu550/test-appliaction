# cypress


### Run front-app
```
npm run build-front
npm run start-front
```


### install dependencies for tests
```shell script
npm install
```


### Run tests
```shell script
npm run clean
npm run test
```

other params
```shell script
npx cypress open
```

### report

```shell script
allure generate _output/allure-results -o _output/allure-report --clean
allure open _output/allure-report
```

https://github.com/cypress-io/cypress-example-recipes/blob/master/examples/logging-in__basic-auth/cypress.config.js

https://docs.cypress.io/guides/references/configuration


https://www.npmjs.com/package/allure-cypress
https://allurereport.org/docs/cypress/

https://github.com/Shelex/cypress-allure-plugin

