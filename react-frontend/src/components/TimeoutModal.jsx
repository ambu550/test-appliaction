import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import setDataTestAttributes from "../helpers/setDataTestAttributes";

const TimeoutModal = ({ showModal, togglePopup, handleStayLoggedIn }) => {
    return <Modal {...setDataTestAttributes("idle-modal")} isOpen={showModal} toggle={togglePopup} keyboard={false} backdrop="static">
        <ModalHeader>Are you still here?</ModalHeader>
        <ModalBody {...setDataTestAttributes("idle-modal-text")}>
            You are inactive. Your session will be disconnected if you do not confirm your presence.
        </ModalBody>
        <ModalFooter>
            <Button color="primary" onClick={handleStayLoggedIn} {...setDataTestAttributes("idle-modal-button")}>I'm here</Button>
        </ModalFooter>
    </Modal>
}

export default TimeoutModal;