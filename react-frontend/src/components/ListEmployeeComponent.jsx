import React, { Component } from 'react'
import EmployeeService from '../services/EmployeeService'
import IdleTimer from 'react-idle-timer';
import setDataTestAttributes from "../helpers/setDataTestAttributes";
import DateTime from "./DateTime";
import TimeoutModal from './TimeoutModal';

class ListEmployeeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                showModal: false,
                employees: []
        }
        this.addEmployee = this.addEmployee.bind(this);
        this.editEmployee = this.editEmployee.bind(this);
        this.deleteEmployee = this.deleteEmployee.bind(this);

        this.idleTimer = null;
        this.logoutTimer = null;
    }

    onIdle = () => {
        this.togglePopup();
        this.logoutTimer = setTimeout(() => {
            this.handleLogout();
        }, 1000 * 60 * 5); // 5 min
    }

    togglePopup = () => {
        this.setState(prevState => ({ showModal: !prevState.showModal }));
    }

    handleStayLoggedIn = () => {
        if (this.logoutTimer) {
            clearTimeout(this.logoutTimer);
            this.logoutTimer = null;
        }
        this.idleTimer.reset();
        this.togglePopup();
    }

    handleLogout = () => {
        this.props.history.push('/wait');
    }

    deleteEmployee(id){
        EmployeeService.deleteEmployee(id).then( res => {
            this.setState({employees: this.state.employees.filter(employee => employee.id !== id)});
        });
    }
    viewEmployee(id){
        this.props.history.push(`/view-employee/${id}`);
    }
    editEmployee(id){
        this.props.history.push(`/add-employee/${id}`);
    }

    componentDidMount(){
        EmployeeService.getEmployees().then((res) => {
            this.setState({ employees: res.data});
        });
    }

    addEmployee(){
        this.props.history.push('/add-employee/_add');
    }


    render() {
        const { showModal } = this.state;

        return (
            <div>
                 <h2 className="text-center">Employees List</h2>
                 <DateTime></DateTime>

                 <div className = "row">
                    <button className="btn btn-primary" {...setDataTestAttributes("addEmployerButton")} onClick={this.addEmployee}> Add Employee</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered" data-qa="employeesTable">

                            <thead>
                                <tr>
                                    <th> Employee First Name</th>
                                    <th> Employee Last Name</th>
                                    <th> Employee Email Id</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.employees.map(
                                        employee => 
                                        <tr key = {employee.id}>
                                             <td> { employee.firstName} </td>   
                                             <td> {employee.lastName}</td>
                                             <td> {employee.emailId}</td>
                                             <td>
                                                 <button onClick={ () => this.editEmployee(employee.id)} className="btn btn-info">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteEmployee(employee.id)} className="btn btn-danger" {...setDataTestAttributes("deleteButton")}>Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewEmployee(employee.id)} className="btn btn-info">View </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>


                <IdleTimer
                    ref={ref => { this.idleTimer = ref }}
                    element={document}
                    stopOnIdle={true}
                    onIdle={this.onIdle}
                    timeout={1000 * 30 * 1} // 30 sec
                />

                <TimeoutModal
                    showModal={showModal}
                    togglePopup={this.togglePopup}
                    handleStayLoggedIn={this.handleStayLoggedIn}
                />

            </div>
        )
    }
}

export default ListEmployeeComponent
