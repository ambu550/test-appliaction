import  React, { useState , useEffect } from 'react'
import setDataTestAttributes from "../helpers/setDataTestAttributes";

export const DateTime = () => {

    const [date, setDate] = useState(new Date());

    useEffect(() => {
        const timer = setInterval(() => setDate(new Date()), 1000);
        return function cleanup() {
            clearInterval(timer)
        }

    });

    return(
        <div>
            <p {...setDataTestAttributes("time")}> Time : {date.toLocaleTimeString('it-IT')}</p>
            <p {...setDataTestAttributes("date")}> Date : {date.toLocaleDateString('en-CA')}</p>

        </div>
    )
}

export default DateTime