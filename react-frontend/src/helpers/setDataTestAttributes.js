const setDataTestAttributes = (name, data = [], fieldName = "value") => ({
    "data-qa": name
});

export default setDataTestAttributes;
