class Fixtures {

    one_record_list = '[{"id": 1, "firstName": "Ivan", "lastName": "Pupkin", "emailId": "pupkin_i@a.com"}]';
    one_record = '{"id": 1, "firstName": "Ivan", "lastName": "Pupkin", "emailId": "pupkin_i@a.com"}';
    three_records = JSON.stringify([
        {
            "id": 1,
            "firstName": "Ivan",
            "lastName": "Pupkin",
            "emailId": "pupkin_i@a.com"
        },
        {
            "id": 2,
            "firstName": "Mikola",
            "lastName": "Vilkin",
            "emailId": "vilkin_m@a.com"
        },
        {
            "id": 3,
            "firstName": "Andrey",
            "lastName": "Rozetkin",
            "emailId": "rozetkin_a@a.com"
        }
    ]);

}

export const fixtures = new Fixtures();
