class Links {

    readonly baseApiLink = 'http://localhost:8080/api/v1';
    readonly listAndPostLink = this.baseApiLink + '/employees';
    readonly updateViewDeletePath = this.listAndPostLink + '/';
}

export const links = new Links();
