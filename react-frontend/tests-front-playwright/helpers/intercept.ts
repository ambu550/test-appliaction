import {Page} from '@playwright/test';
import {allure} from "allure-playwright";
import {links} from "../data/links";

export class Intercept {

    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async logNotMockedRequests() {
        await this.page.route(links.baseApiLink + '/**', (route, request) => {

            allure.step('(NOT MOCKED) API REQUEST to: ' + request.method() + ' ' + request.url(), async () => {
            });

            route.continue();

        });
    }

    waitForApiEmployersList() {
        this.interceptBuilder(links.listAndPostLink, 'GET')
    }

    waitForApiEmployerCreate() {
        this.interceptBuilder(links.listAndPostLink, 'POST')
    }

    waitForApiEmployerDelete(id: number) {
        this.interceptBuilder(links.updateViewDeletePath + id, 'DELETE')
    }


    /**
     * Use it before act to check that act triggering request
     */
    private interceptBuilder(link: string, method: string) {

        allure.step(`[API] Request will exist: ${method} ${link}`, async () => {

            this.page.waitForRequest(request =>
                request.url() === link && request.method() === method,
            );//no await!

        }).then(null);//no await!

    }

}
