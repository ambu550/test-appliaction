import {Page} from '@playwright/test';
import {allure} from "allure-playwright";
import {links} from "../data/links";

export class Mocks {

    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async getEmployeesList(statusCode: number, response: string) {
        await this.mockBuilder(links.listAndPostLink, 'GET', statusCode, response)
    }

    async getEmployer(id: number, statusCode: number, response: string) {
        await this.mockBuilder(links.updateViewDeletePath + id, 'GET', statusCode, response)
    }

    async createEmployer(statusCode: number, response: string) {
        await this.mockBuilder(links.listAndPostLink, 'POST', statusCode, response)

    }

    async updateEmployer(id: number, statusCode: number, response: string) {
        await this.mockBuilder(links.updateViewDeletePath + id, 'POST', statusCode, response)
    }

    async deleteEmployer(id: number, statusCode: number, response: string) {
        await this.mockBuilder(links.updateViewDeletePath + id, 'DELETE', statusCode, response)
    }

    private async mockBuilder(link: string, method: string, statusCode: number, response: string) {

        await allure.step(`(create mock) for link: ${method} ${statusCode} ${link}`, async () => {

            await allure.attachment("response", response, {
                contentType: "application/json",
            });

            await this.page.route(link, async route => {
                if (route.request().method() !== method) {
                    await route.fallback();
                    return;
                }
                await route.fulfill({
                    status: statusCode,
                    contentType: 'application/json',
                    json: JSON.parse(response)
                });

                await allure.step('(mocked) API REQUEST: ' + method + ' ' + link, async () => {
                    await allure.attachment("response " + statusCode, response, {
                        contentType: "application/json",
                    });
                });

            });
        });

    }

}
