import {Page} from '@playwright/test';
import {allure} from "allure-playwright";

/**
 * Don't forget to use
 *
 * await page.clock.install();
 *
 * before page load
 */
export class Timer {

    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async setDateTime(dateTime: string) {
        await allure.step(`Set dateTime to: ${dateTime}`, async () => {
            await this.page.clock.setFixedTime(dateTime);
        });
    }

    async runTime(time: string) {
        await allure.step(`Run time for: ${time}`, async () => {
            await this.page.clock.runFor(time);
        });
    }
}
