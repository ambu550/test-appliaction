import {Locator} from '@playwright/test';
import {allure} from "allure-playwright";
import {readFileSync} from "fs";
import * as path from 'path';


export class AllureHelper {

    readonly allureLinkPath = 'https://gitlab.com/ambu550/test-appliaction/-/issues/';

    async fileToAllure(filePath: string) {
        await allure.attachment(`file: ${filePath}`, readFileSync(path.join(__dirname, filePath)), {
            contentType: "application/json",
        });
    }

    async link(link: string) {
        await allure.link(this.allureLinkPath + link, link);
    }

    async issue(issue: string) {
        await allure.issue(issue, this.allureLinkPath + issue);
    }

    async locatorScreen(locator: Locator) {
        await allure.attachment(`screen-${locator}.png`, await locator.screenshot(), {
            contentType: "image/png",
        });
    }

}

export const allureHelper = new AllureHelper();
