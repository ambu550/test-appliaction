# playwright


### Run front-app

```
npm run build-front
npm run start-front
```

### install dependencies & browsers for tests
```shell script
npm install
npx playwright install 
```


### Run tests

```shell script
npm run clean
npm run test
npm run report
```

other params
```
npx playwright test
npx playwright test --workers 4
npx playwright show-report _output/html-report
npx playwright test --headed
npx playwright test --project=front
npx playwright test --project=front --headed
npx playwright test Steps_usage --project=front
npx playwright test --debug
```

### report

```shell script
allure generate _output/allure-results -o _output/allure-report --clean
allure open _output/allure-report
```


## UI for debug
```shell script
npx playwright test --ui
```

## UI for locators generate
```shell script
npx playwright codegen playwright.dev
```
npx playwright show-report