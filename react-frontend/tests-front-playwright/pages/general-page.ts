import {expect, Page} from '@playwright/test';
import {allure} from 'allure-playwright';
import {AllureHelper} from "../helpers/allure-helper";

export class GeneralPage {

    readonly page: Page;

    constructor(page: Page) {
        this.page = page;
        allure.epic("[front] Tests for UI(playwright)").then(null);
        allure.label(
            "package",
            "employees.front",
        ).then(null);
    }

    // locators

    readonly nameInput = "[name='firstName']";
    readonly lastNameInput = "[name='lastName']";
    readonly emailInput = "[name='emailId']";
    readonly subTitleText = 'Employees List';

    // other

    public async iSeeMainSubtitleText() {
        await this.checkSubtitleText(this.subTitleText)
    }

    private async checkSubtitleText(text: string) {
        await allure.step("[ASSERT] subtitle contains: " + text, async () => {
            await expect(this.page.locator('h2')).toContainText(this.subTitleText);
        });
    }

    public async goToMainPage() {
        await allure.step("Go to main page ", async () => {
            await this.page.goto('/');
        });
    }

    public async goToUrl(url: string) {
        await allure.step(`Go to: ${url}`, async () => {
            await this.page.goto(url);
        });
    }

    public async iSeeUrl(url: string,) {
        await allure.step(`[ASSERT] current URL is: ${url}`, async () => {
            await expect(this.page).toHaveURL(url);
        });
    }

    // form

    public async fillEmployerForm(name: string, lastName: string, email: string,) {
        await allure.step(`Fill form with name: ${name}, lastName: ${lastName}, email: ${email}`, async () => {
            await this.checkFormIsVisible();

            await this.fillInput(this.nameInput, name)
            await this.fillInput(this.lastNameInput, lastName)
            await this.fillInput(this.emailInput, email)
        });
    }

    private async checkFormIsVisible() {
        await allure.step(`[ASSERT] Wait for form is visible`, async () => {
            await expect(this.page.locator('form')).toBeVisible();
        });
    }

    private async fillInput(loc: string, text: string) {
        await allure.step(`Fill input ${loc} with text: ${text}`, async () => {
            await this.page.locator(loc).fill(text);
        });
    }


    // buttons

    public async addEmployerButtonClick() {
            await this.clickButtonByText('Add Employee');
    }

    public async saveButtonClick() {
            await this.clickButtonByText('Save');
    }

    public async cancelButtonClick() {
        await this.clickButtonByText('Cancel');
    }

    public async viewButtonClick(row: number) {
        await this.clickButtonByNameAndRow('View', row);
    }

    public async deleteButtonClick(row: number) {
        await this.clickButtonByDataQaAndRow('deleteButton', row);
    }

    private async clickButtonByNameAndRow(name: string, row: number) {
        await allure.step(`Click button by name: ${name} at row: ${row}`, async () => {
            await this.page.getByRole('button', { name: name }).nth(row-1).click();
        });
    }

    private async clickButtonByDataQaAndRow(dataQa: string, row: number) {
        await allure.step(`Click button by data-qa: ${dataQa} at row: ${row}`, async () => {
            await this.page.getByTestId(dataQa).nth(row-1).click();
        });
    }

    private async clickButtonByText(text: string) {
        await allure.step(`Click button by text: ${text}`, async () => {
            await this.page.getByText(text).click();
        });
    }

    // table

    public async iSeeTableHeadIsVisible() {
        await allure.step(`[ASSERT] Table head is visible`, async () => {
            await expect(this.page.locator('thead')).toBeVisible();
        });
    }

    public async iSeeTableHeadCellHaveText(column: number, text: string) {
       await this.checkTableRow('thead', 'th', 1, column, text);
    }

    public async iSeeTableBodyCellHaveText(row: number, column: number, text: string) {
        await this.checkTableRow('tbody', 'td', row, column, text);
    }

    public async iSeeTableBodyAssertRowsCount(count: number) {
        await allure.step(`[ASSERT] Check table body has ${count} rows`, async () => {
            await expect(this.page.locator('tbody').locator('tr')).toHaveCount(count);
        });
    }

    /**
     * Assert table body cell data by row-email & column
     */
    public async iSeeTableBodyCellByRowNameHaveText(rowEmail: string, column: number, text: string) {
        await this.checkTableRowByRowEmail(rowEmail, column, text);
    }

    private async checkTableRow(part: string, tag: string, row: number, column: number, text: string) {
        await allure.step(`[ASSERT] Check table cell in ${part} , row:${row} column:${column} have text: ${text}`, async () => {
            await expect(this.page.locator(part).locator('tr').nth(row-1)
                .locator(tag).nth(column-1)).toHaveText(text);
        });
    }

    /**
     * For construct (table body by row-name(first column) and column) cell data assert
     */
    private async checkTableRowByRowEmail(rowEmail: string, column: number, text: string) {
        const colName = await this.page.locator('thead').locator('th').nth(column-1).textContent();

        await allure.step(`[ASSERT] Table cell in row-email:<<${rowEmail}>> column:${column}(${colName}) contains text: <<${text}>>`, async () => {
            await expect(this.page.locator('tbody')
                .locator('tr',{ has: this.page.locator(`text="${rowEmail}"`) })
                .locator('td').nth(column-1)).toContainText(text);
        });
    }

    // timer methods

    public async iSeeDate(date: string) {
        await allure.step(`[ASSERT] Page date is: ${date}`, async () => {
            await expect(this.page.getByTestId('date')).toContainText(date)
        });
    }

    public async iSeeTime(date: string) {
        await allure.step(`[ASSERT] Page time is: ${date}`, async () => {
            await expect(this.page.getByTestId('time')).toContainText(date)
        });
    }

    // modal methods

    public async iSeeIdleModalIsVisible() {
        await allure.step(`[ASSERT] Idle modal is visible`, async () => {
            await expect(this.page.getByTestId('idle-modal')).toBeVisible();
        });
    }

    public async clickImHereButton() {
        await allure.step(`Click on "I'm here" button`, async () => {
            await this.page.getByTestId('idle-modal-button').click();
        });
    }

    public async iSeeIdleModalIsAbsent() {
        await allure.step(`[ASSERT] Idle modal is absent`, async () => {
            await expect(this.page.getByTestId('idle-modal')).toHaveCount(0);
        });
    }

    //allure

    public async allureTableScreen() {
        const allureHelper = new AllureHelper();
        await allureHelper.locatorScreen(this.page.locator('table').first());
    }
}