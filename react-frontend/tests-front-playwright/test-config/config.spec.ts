// @ts-check
import {test} from '@playwright/test';
import {allureHelper} from "../helpers/allure-helper";
import {allure} from 'allure-playwright';

//off video & screenshots
test.use({screenshot: 'off', video: 'off'});

test('front-config', async ({}) => {
    await allure.epic("_Config");
    await allure.label(
        "package",
        "InfoTest",
    );
    await allureHelper.fileToAllure("../playwright.config.ts");
    await allureHelper.fileToAllure("../playwright.config-ci.ts");

});