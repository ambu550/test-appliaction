// @ts-check
import {test} from '@playwright/test';
import {GeneralPage} from '../pages/general-page';
import {Mocks} from '../helpers/mocks';
import {Timer} from '../helpers/timer';
import {Intercept} from '../helpers/intercept';
import {allure} from "allure-playwright";


test.beforeEach(async ({page}) => {
    await allure.feature("Time tests");
    const mocks = new Mocks(page);
    const intercept = new Intercept(page);

    await page.clock.install();

    await intercept.logNotMockedRequests();
    await mocks.getEmployeesList(200, '[]');
});

test('Date & time set', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const timer = new Timer(page);

    await timer.setDateTime('2024-02-02 08:00:00');

    await generalPage.goToMainPage();

    await generalPage.iSeeDate('2024-02-02');
    await generalPage.iSeeTime('08:00:00');
});


test('Time run for idle modal test', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const timer = new Timer(page);

    await generalPage.goToMainPage();

    await generalPage.iSeeMainSubtitleText();

    await timer.runTime('05:00');

    await generalPage.iSeeIdleModalIsVisible();
});

test('Time run and idle modal button click', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const timer = new Timer(page);

    await generalPage.goToMainPage();

    await generalPage.iSeeMainSubtitleText()

    await timer.runTime('05:00');

    await generalPage.clickImHereButton();

    await generalPage.iSeeIdleModalIsAbsent();
});

test('Time run and idle modal and wait', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const timer = new Timer(page);

    await generalPage.goToMainPage();

    await generalPage.iSeeMainSubtitleText()

    await timer.runTime('05:00');
    await generalPage.iSeeIdleModalIsVisible();
    await timer.runTime('00:30');

    await generalPage.iSeeUrl(`/wait`);
});