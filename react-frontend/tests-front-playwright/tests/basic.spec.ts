// @ts-check
import {test} from '@playwright/test';
import {GeneralPage} from '../pages/general-page';
import {Mocks} from '../helpers/mocks';
import {Intercept} from '../helpers/intercept';
import {fixtures} from '../data/fixtures';
import {allure} from "allure-playwright";


test.beforeEach(async ({page}) => {
    await allure.feature("Basic tests");

    const intercept = new Intercept(page);
    await intercept.logNotMockedRequests();
});

test('Basic enter page test with empty table', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const mocks = new Mocks(page);
    const intercept = new Intercept(page);

    await mocks.getEmployeesList(200, '[]')

    intercept.waitForApiEmployersList();
    await generalPage.goToMainPage();

    await generalPage.iSeeMainSubtitleText();
    await generalPage.iSeeTableHeadIsVisible();

    await generalPage.allureTableScreen();

    await generalPage.iSeeTableHeadCellHaveText(1, 'Employee First Name');
    await generalPage.iSeeTableHeadCellHaveText(2, 'Employee Last Name');
    await generalPage.iSeeTableHeadCellHaveText(3, 'Employee Email Id');
    await generalPage.iSeeTableHeadCellHaveText(4, 'Actions');
});

test('Basic enter page test without mocks', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const intercept = new Intercept(page);

    intercept.waitForApiEmployersList();
    await generalPage.goToMainPage();
    await generalPage.iSeeMainSubtitleText();

});

test('Basic view employee check', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const mocks = new Mocks(page);

    const id = 1;
    const name = 'Ivan';
    const lastName = 'Pupkin';
    const email = 'pupkin_i@a.com';

    await mocks.getEmployeesList(200, `[{"id": ${id}, "firstName": "${name}", "lastName": "${lastName}", "emailId": "${email}"}]`);
    await mocks.getEmployer(1, 200, `{"id": ${id}, "firstName": "${name}", "lastName": "${lastName}", "emailId": "${email}"}`);
    await generalPage.goToMainPage();
    await generalPage.viewButtonClick(1);
    await generalPage.iSeeUrl(`/view-employee/${id}`);
});


test('Basic filled table rows check', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const mocks = new Mocks(page);

    const id = '1';
    const name = 'Ivan';
    const lastName = 'Pupkin';
    const email = 'pupkin_i@a.com';

    await mocks.getEmployeesList(200, `[{"id": ${id}, "firstName": "${name}", "lastName": "${lastName}", "emailId": "${email}"}]`);

    await generalPage.goToMainPage();

    await generalPage.iSeeTableHeadIsVisible();

    await generalPage.iSeeTableHeadCellHaveText(1, 'Employee First Name');

    await generalPage.iSeeTableBodyAssertRowsCount(1);

    await generalPage.allureTableScreen();

    await generalPage.iSeeTableBodyCellHaveText(1, 1, name);
    await generalPage.iSeeTableBodyCellHaveText(1, 2, lastName);
    await generalPage.iSeeTableBodyCellHaveText(1, 3, email);

});


test('Check filled table', async ({page}) => {
    const generalPage = new GeneralPage(page);
    const mocks = new Mocks(page);

    await mocks.getEmployeesList(200, fixtures.three_records);

    await generalPage.goToUrl('/employees');
    await generalPage.iSeeMainSubtitleText();

    await generalPage.allureTableScreen();

    await generalPage.iSeeTableBodyAssertRowsCount(3);

    await generalPage.iSeeTableBodyCellHaveText(2, 2, 'Vilkin');

    await generalPage.iSeeTableBodyCellByRowNameHaveText('rozetkin_a@a.com', 2, 'Rozetkin');

});
