// @ts-check
import {expect, test} from '@playwright/test';
import {GeneralPage} from '../pages/general-page';
import {Mocks} from '../helpers/mocks';
import {Intercept} from '../helpers/intercept';
import {allure} from "allure-playwright";
import {fixtures} from "../data/fixtures";
import {allureHelper} from "../helpers/allure-helper";

test.beforeEach(async ({page}) => {
    await allure.feature("API interactions");
    await allure.tag("api");

    const intercept = new Intercept(page);
    await intercept.logNotMockedRequests();
});


test('Check empty table, add employer, check API triggers', {tag: '@api'}, async ({page}) => {
    await allureHelper.issue("2");
    const generalPage = new GeneralPage(page);
    const mocks = new Mocks(page);
    const intercept = new Intercept(page);

    const name = 'Ivan';
    const lastName = 'Pupkin';
    const email = 'pupkin_i@a.com';


    await mocks.getEmployeesList(200, '[]')
    await mocks.createEmployer(200, '[]') //not critical

    intercept.waitForApiEmployersList(); //no await!

    await generalPage.goToMainPage();

    await generalPage.addEmployerButtonClick();

    await mocks.getEmployeesList(200, `[{"id": 1, "firstName": "${name}", "lastName": "${lastName}", "emailId": "${email}"}]`);

    await generalPage.fillEmployerForm(name, lastName, email);

    intercept.waitForApiEmployerCreate();
    intercept.waitForApiEmployersList();

    await generalPage.saveButtonClick();

});


test('Check add employer canceled, no POST requests', {tag: '@api'}, async ({page}) => {

    const generalPage = new GeneralPage(page);
    const mocks = new Mocks(page);
    const intercept = new Intercept(page);

    const name = 'Miron';
    const lastName = 'Babkin';
    const email = 'babkin_m@a.com';


    await mocks.getEmployeesList(200, '[]')

    intercept.waitForApiEmployersList(); //no await!

    await generalPage.goToMainPage();

    await generalPage.addEmployerButtonClick();

    await generalPage.fillEmployerForm(name, lastName, email);

    intercept.waitForApiEmployersList();

    await allure.logStep("Prepare POST interceptor");

    let postRequests = 0;
    page.on('request', request => {
        if (request.method() === 'POST')
            postRequests++;
    });

    await generalPage.cancelButtonClick();

    await allure.step("POST requests expected: 0", async () => {
        expect(postRequests).toEqual(0);
    });

});


test('Check API request on employer Delete', async ({page}) => {
    await allureHelper.link("1");
    const generalPage = new GeneralPage(page);
    const mocks = new Mocks(page);
    const intercept = new Intercept(page);

    await mocks.getEmployeesList(200, fixtures.three_records);
    await mocks.deleteEmployer(2, 200, '{"deleted":true}');

    await generalPage.goToMainPage();

    intercept.waitForApiEmployerDelete(2);

    await generalPage.deleteButtonClick(2);

});