#!/bin/bash

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

set -e

#start back-end
./start-back.sh

#start front
./start-front.sh


echo "Application started, go to:"
echo "http://localhost:3000/"