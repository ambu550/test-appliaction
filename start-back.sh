#!/bin/bash

set -e
pwd

# make .env
echo "make local env"
echo CI_DOCKER_DIR=.. > ./springboot-backend/auto-tests/.env

cp ./springboot-backend/src/main/resources/application-test.yml ./springboot-backend/auto-tests/java-test/src/test/resources/config/application-test.yml


# rebuild application in docker
while true; do
    read -p "Rebuild project? (do it in new branch!) y/n: " yn
    case $yn in
        [Yy]* ) docker-compose -f ./springboot-backend/auto-tests/build-app.yml up --force-recreate; break;;
        [Nn]* ) break;;
        * ) echo "Please answer y or n";;
    esac
done

docker-compose -f ./springboot-backend/auto-tests/build-app.yml down --remove-orphans
docker-compose -f ./springboot-backend/auto-tests/docker-compose.yml up -d backend-app



# need to clean logs locally !
for (( count=1; count<3; count++ ))
  do
  echo "get local permissions to logs $MODULE_name"
  sleep 5
  docker exec backend-app chmod 777 -R ./logs
  done

echo "Back-end started"
