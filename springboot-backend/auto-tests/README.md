## Used java-testhelper-core (DEPENDENCY ACCESS BY TOKEN)
=> https://java-testhelper-core-ambu550-1f8723467f72abd0047dfa797c79a09304.gitlab.io
##### examples
=> https://java-testhelper-tests-ambu550-1e56a841797c51161915e4c76af127e54.gitlab.io/



#
# How to start

- copy local build.gradle (local DEPENDENCY required)

```shell script
cp java-test/build.gradle.local java-test/build.gradle
```

- create ci/cd build.gradle (for CI\CD test or local debug)
- $\textcolor{red}{\text{don`t forget manually add TOKEN}}$

```shell script
sed '/mavenLocal()/r config/maven_pull.ci' java-test/build.gradle.local > java-test/build.gradle
```

### To run local env (in docker) for all tests
```shell script
cd ../..
./start-back.sh
```

- run tests locally

```shell script
./gradlew clean :java-test:test 
```

- local report (without server)
```shell script
./gradlew :java-test:downloadAllure
./gradlew :java-test:allureServe
```

