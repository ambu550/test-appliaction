import io.qameta.allure.Epic;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static net.testhelper.allurereporter.AllureReporter.attachFromFile;


@Tag("employees")
@Epic("_Config")
@Order(1)
final public class InfoTest {


    @Test
    @DisplayName("app-config")
    void showConfig() {
        attachFromFile(
                "config",
                "config/application-test.yml");
    }

}
