package employees.api.negative;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.response.Conditions.*;
import static org.hamcrest.Matchers.is;


@Link("4")
@Feature("Get employer; negative")
class GetNegativeTest extends TestsSetupApi {

    @Test
    @Description("""
            Get NOT existing employer""")
    void getNotExistingEmployer() {

        api.getEmployer("15")
                .shouldHave(statusCode(404))
                .bodyShould(bodyJsonContains("""
                        {
                            "message": "Employee not exist with id :15"
                        }
                        """));
    }

    @Test
    @Description("""
            Get NOT existing wrong (zero int) employer""")
    void getWrongZeroIntEmployer() {

        api.getEmployer("0")
                .shouldHave(statusCode(404))
                .bodyShould(bodyJsonContains("""
                        {
                            "message": "Employee not exist with id :0"
                        }
                        """));
    }

}
