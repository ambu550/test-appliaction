package employees.api.negative;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.response.Conditions.*;


@Link("4")
@Feature("Delete employer: negative")
class DeleteNegativeTest extends TestsSetupApi {


    @Test
    @Description("""
            Delete NOT existing employer
            POST 404 -> no new messages in Rabbit""")
    void deleteNotExistingEmployer() {

        api.deleteEmployer("11")
                .shouldHave(statusCode(404))
                .bodyShould(bodyJsonContains("""
                        {
                            "message": "Employee not exist with id :11"
                        }
                        """));

        logReader.checkLog("Start to delete Employee: id:11");
        rabbit.assertMessagesInDeleteQueue(0);
    }

}
