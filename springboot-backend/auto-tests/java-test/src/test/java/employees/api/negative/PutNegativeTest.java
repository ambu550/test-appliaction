package employees.api.negative;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.response.Conditions.*;


@Link("4")
@Feature("Update employer; negative")
class PutNegativeTest extends TestsSetupApi {

    @Test
    @Description("""
            Update existing employer without fields in request
            -> old record left""")
    void updateExistingEmployerWithoutFields() {

        final var id = "1";
        final var nameOld = "Alex";
        final var lastNameOld = "Userov";
        final var emailOld = "alex@com.com";

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", id},
                {"first_name", nameOld},
                {"last_name", lastNameOld},
                {"email_id", emailOld},
        });

        api.putEmployer(id,
                        """
                                {
                                    "firstName": "test1",
                                }""")
                .shouldHave(statusCode(400))
                .bodyShould(bodyJsonContains("""
                        {
                            "type": "about:blank",
                            "title": "Bad Request",
                            "status": 400,
                            "detail": "Failed to read request",
                            "instance": "/api/v1/employees/1"
                        }
                        """));

        sqlMaria.recordEmployees_exist(new String[][]{
                {"id", id},
                {"first_name", nameOld},
                {"last_name", lastNameOld},
                {"email_id", emailOld},
        });
        sqlMaria.recordEmployees_exist(1);
    }

    @Test
    @Description("""
            Update NOT existing employer""")
    void getNotExistingEmployer() {

        api.putEmployer("16", "test1", "test2", "test3")
                .shouldHave(statusCode(404))
                .bodyShould(bodyJsonContains("""
                        {
                            "message": "Employee not exist with id :16"
                        }
                        """));

        sqlMaria.recordEmployees_exist(0);
        logReader.checkLog("Start to update Employee: id:16");
    }

}
