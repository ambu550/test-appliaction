package employees.api;

import io.qameta.allure.Epic;
import io.qameta.allure.Step;
import net.testhelper.brokers.Rabbit;
import net.testhelper.db.MySql;
import net.testhelper.logs.LogTest;
import net.testhelper.services.TestApiService;
import org.junit.jupiter.api.*;


@Tag("employees")
@Epic("[api] Tests for API")
@TestMethodOrder(MethodOrderer.MethodName.class)
public abstract class TestsSetupApi {



    protected final MySql sqlMaria = MySql.getInstance();
    protected final TestApiService api = TestApiService.getInstance();
    protected final LogTest logReader = new LogTest();

    protected final Rabbit rabbit = Rabbit.getInstance();

    @BeforeEach
    @Step("Clean")
    public void clean() {
        sqlMaria.cleanDatabase();
        rabbit.clean();
        logReader.cleanLog();
    }

    @AfterEach
    @Step("Logs")
    public void logs() {
        logReader.readLog();
    }

}
