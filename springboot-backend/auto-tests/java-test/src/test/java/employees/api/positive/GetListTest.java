package employees.api.positive;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.response.Conditions.*;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.is;


@Feature("Get employers list")
class GetListTest extends TestsSetupApi {


    @Test
    @Description("""
            Get list with empty employer""")
    void getEmptyEmployers() {

        api.getEmployersList()
                .shouldHave(statusCode(200))
                .bodyShould(bodyJsonEqual("[]"));
    }

    @Test
    @Description("""
            Get list with one employer""")
    void getOneRecordEmployers() {

        final var id = "1";
        final var name = "Alex";
        final var lastName = "Userov";
        final var email = "alex@com.com";

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", id},
                {"first_name", name},
                {"last_name", lastName},
                {"email_id", email},
        });

        api.getEmployersList()
                .shouldHave(statusCode(200))
                .bodyShould(bodyJsonEqual("""
                        [{
                            "id": 1,
                            "firstName": "Alex",
                            "lastName": "Userov",
                            "emailId": "alex@com.com"
                        }]
                        """));
    }

    @Test
    @Description("""
            Get list with two employers""")
    void getTwoRecordsEmployers() {

        final var name1 = "Alex";
        final var lastName1 = "Userov";
        final var email1 = "alex@com.com";

        final var name2 = "Ivan";
        final var lastName2 = "Pupkin";
        final var email2 = "pup@com.com";

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", "1"},
                {"first_name", name1},
                {"last_name", lastName1},
                {"email_id", email1},
        });

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", "2"},
                {"first_name", name2},
                {"last_name", lastName2},
                {"email_id", email2},
        });

        api.getEmployersList()
                .shouldHave(statusCode(200))
                .bodyShould(bodyJsonStrictOrder("""
                        [{
                            "id": 1,
                            "firstName": "Alex",
                            "lastName": "Userov",
                            "emailId": "alex@com.com"
                        },
                        {
                            "id": 2,
                            "firstName": "Ivan",
                            "lastName": "Pupkin",
                            "emailId": "pup@com.com"
                        }]
                        """));
    }

}
