package employees.api.positive;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.response.Conditions.*;
import static org.hamcrest.Matchers.is;


@Feature("Update employer")
class PutTest extends TestsSetupApi {


    @Test
    @Description("""
            Update existing employer""")
    void updateExistingEmployer() {

        final var id = "1";
        final var nameOld = "Alex";
        final var lastNameOld = "Userov";
        final var emailOld = "alex@com.com";


        final var nameNew = "Alex_new";
        final var lastNameNew= "Userov_new";
        final var emailNew = "alex_new@com.com";


        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", id},
                {"first_name", nameOld},
                {"last_name", lastNameOld},
                {"email_id", emailOld},
        });

        api.putEmployer(id, nameNew, lastNameNew, emailNew)
                .shouldHave(statusCode(200))
                .bodyShould(bodyJsonEqual("""
                        {
                            "id": 1,
                            "firstName": "Alex_new",
                            "lastName": "Userov_new",
                            "emailId": "alex_new@com.com"
                        }
                        """));

        sqlMaria.recordEmployees_exist(new String[][]{
                {"id", id},
                {"first_name", nameNew},
                {"last_name", lastNameNew},
                {"email_id", emailNew},
        });
        sqlMaria.recordEmployees_exist(1);

        logReader.checkLog("Start to update Employee: id:1");
    }
}
