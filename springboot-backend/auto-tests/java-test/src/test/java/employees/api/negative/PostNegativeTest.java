package employees.api.negative;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import net.testhelper.mail.EmailApi;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.response.Conditions.*;
import static org.hamcrest.Matchers.containsString;


@Link("4")
@Feature("Create employer; negative")
class PostNegativeTest extends TestsSetupApi {

    private final EmailApi emailApi = EmailApi.getInstance();

    @Test
    @Issue("2")
    @Description("""
            Create employer with empty name
            POST 400 -> no new records created -> no new messages in Rabbit -> no email""")
    void createEmployerEmptyName() {

        final var lastName = "Userov";
        final var email = "alex@com.com";

        emailApi.clearMailbox(email);

        api.postEmployer("", lastName, email)
                .shouldHave(statusCode(400))
                .bodyShould(bodyJsonContains("""
                        {
                            "errors": [
                                "first_name is required."
                            ]
                        }
                        """));

        sqlMaria.recordEmployees_notExist();
        rabbit.assertMessagesInRegisterQueue(0);
        emailApi.checkMailboxIsEmpty(email);
    }

    @Test
    @Description("""
            Create employer with Wrong email
            POST 400 -> no new records created -> no new messages in Rabbit""")
    void createEmployerWrongEmail() {

        api.postEmployer(
                        """
                                {
                                    "firstName": "Tester",
                                    "lastName": "User",
                                    "emailId": "alexcom.com"
                                }"""
                )
                .shouldHave(statusCode(400))
                .bodyShould(bodyJsonContains("""
                        {
                            "errors": [
                                "The email address is invalid."
                            ]
                        }
                        """));

        rabbit.assertMessagesInRegisterQueue(0);
        sqlMaria.recordEmployees_notExist();
    }

    @Test
    @Description("""
            Create employer with Duplicate email
            POST 400 -> no new records created -> no new messages in Rabbit -> no email""")
    void createEmployerEmailAlreadyExist() {

        final var emailOld = "alex@com.com";

        emailApi.clearMailbox(emailOld);

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", "1"},
                {"first_name", "Some"},
                {"last_name", "User"},
                {"email_id", emailOld},
        });

        api.postEmployer("Alex", "Pupkin", emailOld)
                .shouldHave(statusCode(400))
                .shouldHave(bodyField("message", containsString("Duplicate entry 'alex@com.com'")));

        sqlMaria.recordEmployees_exist(1);
        rabbit.assertMessagesInRegisterQueue(0);
        emailApi.checkMailboxIsEmpty(emailOld);
    }

}
