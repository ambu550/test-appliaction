package employees.api.positive;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import net.testhelper.mail.EmailApi;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.list.ListConditions.jsonEquals;
import static net.testhelper.conditions.response.Conditions.*;


@Feature("Create employer")
@Link("5")
class PostTest extends TestsSetupApi {

    private final EmailApi emailApi = EmailApi.getInstance();

    @Test
    @Description("""
            Create valid employer
            POST -> save to DB -> push message to Rabbit (message check)
            -> email sent from HR""")
    void createEmployer() {

        final var name = "Alex";
        final var lastName = "Userov";
        final var email = "alex@com.com";

        emailApi.clearMailbox(email);

        api.postEmployer(name, lastName, email)
                .shouldHave(statusCode(201))
                .bodyShould(bodyJsonEqual("""
                        {
                            "id": 1,
                            "firstName": "Alex",
                            "lastName": "Userov",
                            "emailId": "alex@com.com"
                        }
                        """));

        sqlMaria.recordEmployees_exist(new String[][]{
                {"first_name", name},
                {"last_name", lastName},
                {"email_id", email},
        });

        rabbit.grabMessageFromRegisterQueue()
                        .test(jsonEquals("""
                        {
                            "id": 1,
                            "firstName": "Alex",
                            "lastName": "Userov",
                            "emailId": "alex@com.com"
                        }"""));

        emailApi.assertCountMessages(email, 1);
        emailApi.checkLatestEmail(
                email,
                "Success registration",
                "hr@company.com",
                "Dear Alex Userov, welcome to our company"
                );

        logReader.checkLog("Start to create Employee: Alex Userov");
        logReader.checkLog("Employee created: Alex Userov");
    }

    @Test
    @Description("""
            Create employer with cases:
            email to lowercase
            first & Last names capitalize first letter
            POST -> save to DB -> push message to Rabbit (message check)
            -> email sent from HR""")
    void createEmployerWithCases() {

        final var email = "PUpkiN@com.com";
        emailApi.clearMailbox(email);

        api.postEmployer("""
                        {
                            "firstName": "ivaN",
                            "lastName": "puPkin",
                            "emailId": "PUpkiN@com.coM"
                        }
                        """)
                .shouldHave(statusCode(201))
                .bodyShould(bodyJsonEqual("""
                        {
                            "id": 1,
                            "firstName": "Ivan",
                            "lastName": "Pupkin",
                            "emailId": "pupkin@com.com"
                        }
                        """));

        sqlMaria.recordEmployees_exist(new String[][]{
                {"first_name", "Ivan"},
                {"last_name", "Pupkin"},
                {"email_id", "pupkin@com.com"},
        });

        rabbit.assertMessagesInRegisterQueue(1);
        emailApi.checkLatestEmail(
                email,
                "Success registration",
                "hr@company.com",
                "Dear Ivan Pupkin, welcome to our company"
        );
    }
}
