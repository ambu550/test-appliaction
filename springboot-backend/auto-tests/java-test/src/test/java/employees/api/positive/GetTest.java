package employees.api.positive;

import employees.api.TestsSetupApi;
import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.response.Conditions.*;
import static org.hamcrest.Matchers.is;


@Feature("Get employer")
class GetTest extends TestsSetupApi {


    @Test
    @Description("""
            Get existing employer""")
    void getExistingEmployer() {

        final var id = "1";
        final var name = "Alex";
        final var lastName = "Userov";
        final var email = "alex@com.com";

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", id},
                {"first_name", name},
                {"last_name", lastName},
                {"email_id", email},
        });

        api.getEmployer(id)
                .shouldHave(statusCode(200))
                .bodyShould(bodyJsonEqual("""
                        {
                            "id": 1,
                            "firstName": "Alex",
                            "lastName": "Userov",
                            "emailId": "alex@com.com"
                        }
                        """));
    }

    @Test
    @Description("""
            Get existing employer2""")
    void getExistingEmployer2() {

        final var id = "22";
        final var name = "Ivan";
        final var lastName = "Pupkin";
        final var email = "pup@com.com";

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", id},
                {"first_name", name},
                {"last_name", lastName},
                {"email_id", email},
        });

        api.getEmployer(id)
                .shouldHave(statusCode(200))
                .shouldHave(bodyField("firstName", is(name)))
                .shouldHave(bodyField("lastName", is(lastName)))
                .shouldHave(bodyField("emailId", is(email)));
    }

}
