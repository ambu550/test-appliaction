package employees.api.positive;

import employees.api.TestsSetupApi;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import net.testhelper.mail.EmailApi;
import org.junit.jupiter.api.Test;

import static net.testhelper.conditions.list.ListConditions.jsonEquals;
import static net.testhelper.conditions.response.Conditions.*;
import static org.hamcrest.Matchers.is;


@Feature("Delete employer")
@Link("5")
class DeleteTest extends TestsSetupApi {

    private final EmailApi emailApi = EmailApi.getInstance();

    @Test
    @Description("""
            Delete existing employer
            DELETE -> delete from DB -> push message to Rabbit (message check) -> email from director""")
    void deleteExistingEmployer() {

        final var id = "3";
        final var name = "Alex";
        final var lastName = "Userov";
        final var email = "alex@com.com";

        emailApi.clearMailbox(email);

        sqlMaria.insertIntoEmployees(new String[][]{
                {"id", id},
                {"first_name", name},
                {"last_name", lastName},
                {"email_id", email},
        });

        api.deleteEmployer(id)
                .shouldHave(statusCode(200))
                .bodyShould(bodyJsonEqual("""
                        {
                            "deleted": true
                        }
                        """));

        sqlMaria.recordEmployees_notExist();

        logReader.checkLog("Start to delete Employee: id:3");

        rabbit.assertMessagesInDeleteQueue(1);
        rabbit.grabMessageFromDeleteQueue()
                .test(jsonEquals("""
                        {
                            "id": 3,
                            "emailId": "alex@com.com"
                        }"""));

        emailApi.assertCountMessages(email, 1);
        emailApi.checkLatestEmail(
                email,
                "Success dismissal",
                "director@company.com",
                "Sorry Alex Userov, we say good bye"
        );

        logReader.checkLog("Employee deleted: Alex Userov");
    }

}
