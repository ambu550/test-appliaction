package net.testhelper.brokers;

import net.testhelper.ProjectConfig;
import net.testhelper.assertions.AssertableStringList;
import net.testhelper.modules.rabbit.RabbitMQ;
import org.aeonbits.owner.ConfigFactory;

public class Rabbit{

    final ProjectConfig conf = ConfigFactory.create(ProjectConfig.class, System.getProperties());

    private static Rabbit INSTANCE;
    private final RabbitMQ rabbit;
    private final String registerQueue;
    private final String deleteQueue;


    private Rabbit() {
        rabbit = new RabbitMQ(
                conf.rabbitHost(),
                conf.rabbitPort(),
                conf.rabbitUser(),
                conf.rabbitPass());

        String exchange = conf.exchange();
        String registerKey = conf.registerKey();
        String deleteKey = conf.deleteKey();

        this.deleteQueue = conf.deleteQueue();
        this.registerQueue = conf.registerQueue();

    // declare test exchange, queue, bindings
        rabbit.declareExchange(exchange, "topic");
        rabbit.declareQueue(registerQueue);
        rabbit.declareQueue(deleteQueue);
        rabbit.bindQueue(registerQueue, exchange, registerKey);
        rabbit.bindQueue(deleteQueue, exchange, deleteKey);
    }

    public static Rabbit getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Rabbit();
        }

        return INSTANCE;
    }

    public void clean(){
        rabbit.purgeQueue(registerQueue);
        rabbit.purgeQueue(deleteQueue);
    }

    public void assertMessagesInRegisterQueue(int expectCount){
        rabbit.assertCountInQueue(registerQueue, expectCount);
    }

    public AssertableStringList grabMessageFromRegisterQueue(){
        return rabbit.grabLastMessagesFromQueue(registerQueue);
    }

    public void assertMessagesInDeleteQueue(int expectCount){
        rabbit.assertCountInQueue(deleteQueue, expectCount);
    }

    public AssertableStringList grabMessageFromDeleteQueue(){
        return rabbit.grabLastMessagesFromQueue(deleteQueue);
    }
}

