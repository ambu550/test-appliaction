package net.testhelper;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:config.properties"})
public interface ProjectConfig extends Config {

    @DefaultValue("local")
    String app();

    @Key("${app}.baseUrl")
    String baseUrl();

    @Key("${app}.port")
    int port();

    @Key("${app}.jdbcMySqlUrl")
    String jdbcMySqlUrl();

    @Key("${app}.jdbcMySqlUsername")
    String jdbcMySqlUsername();

    @Key("${app}.jdbcMySqlPassword")
    String jdbcMysqlPassword();

    @Key("${app}.jdbcMySqlSchema")
    String jdbcMySqlSchema();

    @Key("${app}.rabbitHost")
    String rabbitHost();

    @Key("${app}.rabbitPort")
    int rabbitPort();

    @Key("${app}.rabbitUser")
    String rabbitUser();

    @Key("${app}.rabbitPass")
    String rabbitPass();

    @Key("exchange")
    String exchange();

    @Key("registerQueue")
    String registerQueue();

    @Key("registerKey")
    String registerKey();

    @Key("deleteQueue")
    String deleteQueue();

    @Key("deleteKey")
    String deleteKey();

    @Key("${app}.emailApiUrl")
    String emailApiUrl();

    @Key("${app}.emailApiPort")
    int emailApiPort();


}
