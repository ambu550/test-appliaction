package net.testhelper.services;

import io.qameta.allure.Step;
import io.restassured.specification.RequestSpecification;
import net.testhelper.ProjectConfig;
import net.testhelper.assertions.AssertableResponse;
import net.testhelper.modules.api.ApiService;
import org.aeonbits.owner.ConfigFactory;

import java.util.Map;

import static net.testhelper.mappers.StringMappers.stringMapper;


public class TestApiService extends ApiService {

    private final ProjectConfig conf = ConfigFactory.create(ProjectConfig.class, System.getProperties());

    private static TestApiService INSTANCE;
    // private final String myCookie;

    private TestApiService() {
//        myCookie = setupApiAdmin()
//                .when()
//                .get("get_cookie").getCookie("JSESSIONID");
    }

    public static TestApiService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TestApiService();
        }

        return INSTANCE;
    }

    protected RequestSpecification setupJson() {
        return setupBase()
                .basePath("/api/v1")
                // .cookie("JSESSIONID", myCookie)
                .baseUri(conf.baseUrl())
                .port(conf.port());
    }


    @Step("(API) GET employer")
    public AssertableResponse getEmployer(String id) {
        return new AssertableResponse(
                setupJson()
                        .when()
                        .get("/employees/" + id));
    }

    @Step("(API) GET employers list")
    public AssertableResponse getEmployersList() {
        return new AssertableResponse(
                setupJson()
                        .when()
                        .get("/employees"));
    }

    @Step("(API) POST employer")
    public AssertableResponse postEmployer(String firstName, String lastName, String emailId) {
        return new AssertableResponse(
                setupJson()
                        .when()
                        .body(stringMapper("""
                                {"firstName":"${firstName}","lastName":"${lastName}","emailId":"${emailId}"}""",
                                Map.of(
                                        "firstName", firstName,
                                        "lastName", lastName,
                                        "emailId", emailId
                                )))
                        .post("/employees"));
    }

    @Step("(API) POST employer")
    public AssertableResponse postEmployer(String body) {
        return new AssertableResponse(
                setupJson()
                        .when()
                        .body(body)
                        .post("/employees"));
    }

    @Step("(API) PUT employer")
    public AssertableResponse putEmployer(String id, String firstName, String lastName, String emailId) {
        return new AssertableResponse(
                setupJson()
                        .when()
                        .body(stringMapper("""
                                {"firstName":"${firstName}","lastName":"${lastName}","emailId":"${emailId}"}""",
                                Map.of(
                                        "firstName", firstName,
                                        "lastName", lastName,
                                        "emailId", emailId
                                )))
                        .put("/employees/" + id ));
    }

    @Step("(API) PUT employer")
    public AssertableResponse putEmployer(String id, String body) {
        return new AssertableResponse(
                setupJson()
                        .when()
                        .body(body)
                        .put("/employees/" + id ));
    }

    @Step("(API) DELETE employer")
    public AssertableResponse deleteEmployer(String id) {
        return new AssertableResponse(
                setupJson()
                        .when()
                        .delete("/employees/" + id));
    }
}
