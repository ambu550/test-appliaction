package net.testhelper.mail;


import io.qameta.allure.Step;
import io.restassured.specification.RequestSpecification;

import net.testhelper.ProjectConfig;
import net.testhelper.assertions.AssertableResponse;
import net.testhelper.conditions.response.impl.BodyJsonEqualCondition;
import net.testhelper.modules.api.ApiService;
import org.aeonbits.owner.ConfigFactory;

import static net.testhelper.conditions.response.Conditions.bodyField;
import static net.testhelper.conditions.response.Conditions.statusCode;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;


public class EmailApi extends ApiService {

    ProjectConfig conf = ConfigFactory.create(ProjectConfig.class, System.getProperties());


    private static EmailApi INSTANCE;


    private EmailApi() {
    }

    public static EmailApi getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EmailApi();
        }

        return INSTANCE;
    }

    private RequestSpecification setupEmail() {
        return setupBase()
                .baseUri(conf.emailApiUrl())
                .port(conf.emailApiPort());
    }


    @Step("(Email) Clear mailbox: {mailbox}")
    public AssertableResponse clearMailbox(String mailbox) {
        return new AssertableResponse(
                setupEmail()
                        .when()
                        .delete("/api/v1/mailbox/" + mailbox))
                .shouldHave(statusCode(200));
    }

    @Step("(Email)[ASSERT] Count of emails in: {mailbox} = {expectedCount}")
    public void assertCountMessages(String mailbox, int expectedCount) {
        setupEmail()
                .get("/api/v1/mailbox/" + mailbox)
                .then().assertThat()
                .body("size()", is(expectedCount));
    }


    @Step("(Email) Get latest email in: {mailbox}")
    private AssertableResponse getLatestEmail(String mailbox) {
        return new AssertableResponse(
                setupEmail()
                        .when()
                        .get("/api/v1/mailbox/" + mailbox + "/latest"));
    }

    @Step("(Email)[ASSERT] Check latest email {mailbox}")
    public void checkLatestEmail(String mailbox,
                                 String subject,
                                 String from,
                                 String text) {

        getLatestEmail(mailbox)
                .shouldHave(statusCode(200))
                .shouldHave(bodyField("subject", is(subject)))
                .shouldHave(bodyField("header.From[0]", is(from)))
                .shouldHave(bodyField("body.text", containsString(text)));
    }

    @Step("(Email)[ASSERT] Check mailbox is empty: {mailbox}")
    public void checkMailboxIsEmpty(String mailbox) {

        getMailboxEmailsList(mailbox)
                .bodyShould(new BodyJsonEqualCondition("[]"));
    }

    @Step("(Email) Get list of emails from {mailbox}")
    private AssertableResponse getMailboxEmailsList(String mailbox) {
        return new AssertableResponse(
                setupEmail()
                        .get("/api/v1/mailbox/" + mailbox));
    }


}
