package net.testhelper.logs;

import net.testhelper.modules.logs.LogReader;

public class LogTest extends LogReader {

    private final String testLogPath = "logs/app/server.log";

    public void readLog() {
        getAllLogs(testLogPath);
    }

    public void cleanLog() {
        cleanLogs(testLogPath);
    }

    public void addLog(String message) {
        addToLogs(testLogPath, message);
    }

    public void checkLog(String message) {
        existLogsWithAwait(testLogPath, message);
    }

}
