package net.testhelper.db;


import io.qameta.allure.Step;
import net.testhelper.ProjectConfig;
import net.testhelper.modules.db.MySQL;
import org.aeonbits.owner.ConfigFactory;


public class MySql {

    final ProjectConfig conf = ConfigFactory.create(ProjectConfig.class, System.getProperties());

    private final String SCHEMA = conf.jdbcMySqlSchema();

    private final MySQL jdbc;

    private static MySql INSTANCE;

    private MySql() {
        jdbc = new MySQL(
                conf.jdbcMySqlUrl(),
                conf.jdbcMySqlUsername(),
                conf.jdbcMysqlPassword());
    }

    public static MySql getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MySql();
        }

        return INSTANCE;
    }

    //  POST

    public void insertIntoEmployees(String[][] testArray) {

        final String tableName = SCHEMA + ".employees";
        final String[][] TEMPLATE = {
                {"id", "100"},
                {"email_id", "test@a.com"},
                {"first_name", "test_name"},
                {"last_name", "test_last_name"},
        };

        jdbc.insertIntoTable(tableName, TEMPLATE, testArray);
    }

    @Step("Clean all tables")
    public void cleanDatabase() {
        jdbc.truncateTable(SCHEMA + ".employees");
    }

    // GET

    public void recordEmployees_exist(int expectedCount) {
        jdbc.assertCountInTable
                (SCHEMA + ".employees", expectedCount);
    }

    public void recordEmployees_exist(String[][] conditionsArray) {
        jdbc.assertCountInTable
                (SCHEMA + ".employees",
                        conditionsArray, 1);
    }

    public void recordEmployees_notExist() {
        jdbc.assertCountInTable
                (SCHEMA + ".employees", 0);
    }



}
