
### Back-end part

```shell script
cd ..
./start-back.sh
```

## Specifications
https://gitlab.com/ambu550/test-appliaction/-/wikis/REST-API

## USE:

- localhost:3306 (Mysql root:root schema:employee_management_system)
- http://localhost:15672 (RabbitMQ guest_user:guest_pass)
- http://localhost:9000/monitor (Email UI)


### dev mode
(services in docker)
```
docker-compose -f ./auto-tests/docker-compose.yml up -d --force-recreate mysql test-rabbit inbucket
```
```
mvn spring-boot:run
```