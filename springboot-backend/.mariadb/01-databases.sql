-- # create databases
CREATE DATABASE IF NOT EXISTS employee_management_system;

-- # create test_user user and grant rights
CREATE USER 'test_user'@'%' IDENTIFIED BY 'test_pass';
GRANT ALL PRIVILEGES ON *.* TO 'test_user'@'%';
FLUSH PRIVILEGES;
USE employee_management_system;
