package net.javaguides.springboot.helpers;

public class Helper {

    public static String capitalize(String str){
        return str.substring(0, 1).toUpperCase() + str.toLowerCase().substring(1);
    }

}
