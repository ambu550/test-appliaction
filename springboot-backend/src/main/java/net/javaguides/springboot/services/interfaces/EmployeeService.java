package net.javaguides.springboot.services.interfaces;

import net.javaguides.springboot.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    Employee create(Employee employee);
    Employee save(Employee employee);
    void deleteById(long id);
    List<Employee> findAll();
    Optional<Employee> findById(long id);
}