package net.javaguides.springboot.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class RabbitSenderService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${rabbit.exchange}")
    private String exchangeName;

    @Value("${rabbit.keys.add}")
    private String addKey;

    @Value("${rabbit.keys.delete}")
    private String deleteKey;

    public void sendRegisterMessage(
            int id, String email, String firstName, String lastName) {

        try {
            rabbitTemplate.convertAndSend(exchangeName, addKey, String.format("""
                            {"id": %d, "firstName": "%s", "lastName": "%s", "emailId": "%s"}""",
                    id, firstName,
                    lastName, email));
            log.info("Message successfully sent to rabbitMQ by employer: {}", email);
        } catch (Exception e) {
            log.error("Failed to send message to rabbitMQ with key:<{}> while register employer: {}",
                    addKey, email);
            log.error(e);
        }
    }

    public void sendDismissalMessage(
            int id, String email) {

        try {
            rabbitTemplate.convertAndSend(exchangeName, deleteKey, String.format("""
                            {"id": %d, "emailId": "%s"}""",
                    id, email));
        } catch (Exception e) {
            log.error("Failed to send message with key:<{}> about employer:{} deletion",
                    deleteKey, email);
        }
    }

}
