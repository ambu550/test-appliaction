package net.javaguides.springboot.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class MailSenderService {

    @Autowired
    private JavaMailSender emailSender;

    public void sendRegisterMail(
            String to, String firstName, String lastName) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("hr@company.com");
        message.setTo(to);
        message.setSubject("Success registration");
        message.setText(String.format("Dear %s %s, welcome to our company",
                firstName, lastName));

        sendEmail(to, message);
    }

    public void sendDismissalMail(
            String to, String firstName, String lastName) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("director@company.com");
        message.setTo(to);
        message.setSubject("Success dismissal");
        message.setText(String.format("Sorry %s %s, we say good bye",
                firstName, lastName));

        sendEmail(to, message);
    }

    public void sendEmail(
            String to, SimpleMailMessage message) {

        message.setTo(to);

        try {
            emailSender.send(message);
            log.info("Email successfully sent:<{}>", to);
        }catch (Exception e){
            log.error("Failed to send email:<{}> ", to);
            log.error(e);
        }

    }
}
