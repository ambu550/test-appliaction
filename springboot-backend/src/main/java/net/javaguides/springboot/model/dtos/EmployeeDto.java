package net.javaguides.springboot.model.dtos;

import net.javaguides.springboot.model.Employee;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import static net.javaguides.springboot.helpers.Helper.capitalize;

@Data
public class EmployeeDto {

	@NotBlank(message = "first_name is required.")
	private String firstName;

	@NotNull(message = "last_name is required.")
	private String lastName;

	@Email(message = "The email address is invalid.", flags = { Pattern.Flag.CASE_INSENSITIVE })
	private String emailId;
	
	public Employee toEmployee() {
		return new Employee()
				.setFirstName(capitalize(firstName))
				.setLastName(capitalize(lastName))
				.setEmailId(emailId.toLowerCase());
	}

}
