package net.javaguides.springboot.configuration;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MQConfiguration {

    @Value("${rabbit.exchange}")
    private String exchangeName;
    @Value("${rabbit.keys.add}")
    private String addKey;
    @Value("${rabbit.keys.delete}")
    private String deleteKey;

    @Value("${rabbit.queues.add}")
    private String queueRegister;
    @Value("${rabbit.queues.delete}")
    private String queueDelete;


    @Bean
    public Queue registerQueue() {
        return new Queue(queueRegister, false);
    }
    @Bean
    public Queue deleteQueue() {
        return new Queue(queueDelete, false);
    }
    @Bean
    Exchange exchange() {
        return new TopicExchange(exchangeName, false, false);
    }
    @Bean
    public Declarables bindings() {

        return new Declarables(
                BindingBuilder
                        .bind(registerQueue())
                        .to(exchange())
                        .with(addKey)
                        .noargs(),
                BindingBuilder
                        .bind(deleteQueue())
                        .to(exchange())
                        .with(deleteKey)
                        .noargs());
    }
}
