package net.javaguides.springboot.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import net.javaguides.springboot.model.dtos.EmployeeDto;
import net.javaguides.springboot.services.MailSenderService;
import net.javaguides.springboot.services.RabbitSenderService;
import net.javaguides.springboot.services.interfaces.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Employee;

@Log4j2
@RestController
@RequestMapping("/api/v1/")
public class EmployeeController {


    private final EmployeeService employeeService;
    private final RabbitSenderService rabbitSenderService;
    private final MailSenderService mailService;


    public EmployeeController(EmployeeService employeeService, RabbitSenderService rabbitSenderService, MailSenderService mailService) {
        this.employeeService = employeeService;
        this.rabbitSenderService = rabbitSenderService;
        this.mailService = mailService;
    }

    // get all employees
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        return new ResponseEntity<>(employeeService.findAll(), HttpStatus.OK);
    }

    // create employee rest api
    @PostMapping("/employees")
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody EmployeeDto employeeDto) {

        log.info("Start to create Employee: {} {}", employeeDto.getFirstName(), employeeDto.getLastName());

        Employee createEmployee = employeeService.create(employeeDto.toEmployee());

        log.info("Employee created: {} {}",
                employeeDto.getFirstName(), employeeDto.getLastName());


        rabbitSenderService.sendRegisterMessage(createEmployee.getId(), createEmployee.getEmailId(), createEmployee.getFirstName(), createEmployee.getLastName());
        mailService.sendRegisterMail(createEmployee.getEmailId(), createEmployee.getFirstName(), createEmployee.getLastName());

        return new ResponseEntity<>(createEmployee, HttpStatus.CREATED);
    }

    // get employee by id rest api
    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(
            @PathVariable long id) throws ResourceNotFoundException {

        Optional<Employee> getEmployeeById = employeeService.findById(id);

        if (getEmployeeById.isEmpty()) {
            throw new ResourceNotFoundException("Employee not exist with id :" + id);
        }

        return new ResponseEntity<>(getEmployeeById.get(), HttpStatus.OK);
    }

    // update employee rest api
    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable long id, @RequestBody EmployeeDto employeeDto) {
        log.info("Start to update Employee: id:{}", id);

        Optional<Employee> getEmployeeById = employeeService.findById(id);

        if (getEmployeeById.isEmpty()) {
            throw new ResourceNotFoundException("Employee not exist with id :" + id);
        }

        getEmployeeById.get().setFirstName(employeeDto.getFirstName());
        getEmployeeById.get().setLastName(employeeDto.getLastName());
        getEmployeeById.get().setEmailId(employeeDto.getEmailId());

        Employee updateEmployee = employeeService.save(getEmployeeById.get());
        log.info("Employee updated: id:{}", id);

        return new ResponseEntity<>(updateEmployee, HttpStatus.OK);
    }

    // delete employee rest api
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable long id) {
        log.info("Start to delete Employee: id:{}", id);

        Optional<Employee> getEmployeeById = employeeService.findById(id);

        if (getEmployeeById.isEmpty()) {
            throw new ResourceNotFoundException("Employee not exist with id :" + id);
        }

        employeeService.deleteById(id);

        log.info("Employee deleted: {} {}",
                getEmployeeById.get().getFirstName(), getEmployeeById.get().getLastName());

        rabbitSenderService.sendDismissalMessage(getEmployeeById.get().getId(), getEmployeeById.get().getEmailId());
        mailService.sendDismissalMail(getEmployeeById.get().getEmailId(), getEmployeeById.get().getFirstName(), getEmployeeById.get().getLastName());

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
